#include <cstdio>
#include "decoder.h"
#include "zmq.h"
#include "pmt/pmt.h"
#include "gnuradio/tags.h"
#include <cstring>
#include <sstream>

#define GR_HEADER_MAGIC 0x5FF0
#define GR_HEADER_VERSION 0x01

/** @brief gnuradio ZMQ wireprotocol structure */

typedef struct __attribute__((__packed__))
{
    uint16_t magic;
    uint8_t header_version;
    uint64_t offset;
    uint64_t ntags;
}sZMQ_wire_t,*psZMQ_wire_t;

/** @brief Program working mode enumeration */

enum ProgramMode {
    STANDARD_MODE         = 0,
    READ_FROM_BINARY_FILE = 1,
    SAVE_TO_BINARY_FILE   = 2,
    RX_PACKED             = 4,
};

/** @brief Interrupt flag */

static volatile int gSigintFlag = 0;

/**
 * @brief Handle SIGINT to clean up
 *
 */

static void sigint_handler(int val)
{
    gSigintFlag = 1;
}

struct membuf : std::streambuf {
    membuf(void* b, size_t len)
    {
        char* bc = static_cast<char*>(b);
        this->setg(bc, bc, bc + len);
    }
};

/**
 * @brief Decoder program entry point
 *
 * Reads demodulated values from UDP port 42000 coming from physical demodulator
 * Writes decoded frames to UDP port 42100 to tetra interpreter
 *
 * Filtering log for SDS: sed -n '/SDS/ p' log.txt > out.txt
 *
 */

int main(int argc, char * argv[])
{
    // connect interrupt Ctrl-C handler
    struct sigaction sa;
    sa.sa_handler = sigint_handler;
    sigaction(SIGINT, &sa, 0);

    int udpPortRx = 42000;                                                      // UDP RX port (ie. where to receive bits from PHY layer)
    int udpPortTx = 42100;                                                      // UDP TX port (ie. where to send Json data)

    //in_addr_t interfaceAdress;

    const int FILENAME_LEN = 256;
    char optFilenameInput[FILENAME_LEN]  = "";                                  // input bits filename
    char optFilenameOutput[FILENAME_LEN] = "";                                  // output bits filename

    int programMode = STANDARD_MODE;
    int debugLevel = 1;
    bool bRemoveFillBits = true;
    bool bEnableWiresharkOutput = false;
    bool bEnableMulticast = false;
    bool bUseZMQ = false;

    void *context;
    void *zmqrx;

    int option;
    while ((option = getopt(argc, argv, "hPwr:t:i:o:d:fMZ")) != -1)
    {
        switch (option)
        {
        case 'r':
            udpPortRx = atoi(optarg);
            break;

        case 't':
            udpPortTx = atoi(optarg);
            break;

        case 'P':
            programMode |= RX_PACKED;
            break;
        case 'i':
            strncpy(optFilenameInput, optarg, FILENAME_LEN - 1);
            programMode |= READ_FROM_BINARY_FILE;
            break;

        case 'o':
            strncpy(optFilenameOutput, optarg, FILENAME_LEN - 1);
            programMode |= SAVE_TO_BINARY_FILE;
            break;

        case 'd':
            debugLevel = atoi(optarg);
            break;

        case 'f':
            bRemoveFillBits = false;
            break;

        case 'w':
            bEnableWiresharkOutput = true;
            break;

        case 'Z':
        	bUseZMQ = true;
        	break;

        case 'M':
            bEnableMulticast = true;
            break;

        case 'h':
            printf("\nUsage: ./decoder [OPTIONS]\n\n"
                   "Options:\n"
                   "  -r <UDP socket> receiving from phy [default port is 42000]\n"
                   "  -t <UDP socket> sending Json data [default port is 42100]\n"
                   "  -i <file> replay data from binary file instead of UDP\n"
                   "  -o <file> record data to binary file (can be replayed with -i option)\n"
                   "  -d <level> print debug information\n"
                   "  -f keep fill bits\n"
                   "  -w enable wireshark output [EXPERIMENTAL]\n"
                   "  -P pack rx data (1 byte = 8 bits)\n"
            	   "  -M enable Multicast\n"
            	   "  -Z use ZMQ for data from phy\n"
                   "  -h print this help\n\n");
            exit(EXIT_FAILURE);
            break;

        case '?':
            printf("unkown option, run ./decoder -h to list available options\n");
            exit(EXIT_FAILURE);
            break;
        }
    }


    // create output destination socket
    struct sockaddr_in addr_output;
    memset(&addr_output, 0, sizeof(struct sockaddr_in));
    addr_output.sin_family = AF_INET;
    addr_output.sin_port = htons(udpPortTx);
    inet_aton("239.255.200.8", &addr_output.sin_addr);

    int udpSocketFd  = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
    struct ip_mreq mreq;
    memset(&mreq, 0, sizeof(mreq));
    mreq.imr_interface.s_addr = inet_addr("127.0.0.1");
    if (
        setsockopt(
                udpSocketFd, IPPROTO_IP, IP_MULTICAST_IF, (char*) &mreq, sizeof(mreq)
        ) < 0
    ){
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }
    connect(udpSocketFd, (struct sockaddr *) & addr_output, sizeof(struct sockaddr));
    printf("Output socket 0x%04x on port %d\n", udpSocketFd, udpPortTx);
    if (udpSocketFd < 0)
    {
        perror("Couldn't create output socket");
        exit(EXIT_FAILURE);
    }

    // create decoder
    Tetra::LogLevel logLevel;
    switch (debugLevel)
    {
    case 0:
        logLevel = Tetra::LogLevel::NONE;
        break;
    case 1:
        logLevel = Tetra::LogLevel::LOW;
        break;
    case 2:
        logLevel = Tetra::LogLevel::MEDIUM;
        break;
    case 3:
        logLevel = Tetra::LogLevel::HIGH;
        break;
    case 4:
        logLevel = Tetra::LogLevel::VERYHIGH;
        break;
    default:
        logLevel = Tetra::LogLevel::LOW;

    }

    // output file if any
    int fdOutputSaveFile = 0;

    if (programMode & SAVE_TO_BINARY_FILE)
    {
        // save input bits to file
        fdOutputSaveFile = open(optFilenameOutput, O_RDWR | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP);
        if (fdOutputSaveFile < 0)
        {
            fprintf(stderr, "Couldn't open output file");
            exit(EXIT_FAILURE);
        }
    }

    // input source
    int fdInput = 0;

    if (programMode & READ_FROM_BINARY_FILE)
    {
        // read input bits from file
        fdInput = open(optFilenameInput, O_RDONLY);

        printf("Input from file '%s' 0x%04x\n", optFilenameInput, fdInput);

        if (fdInput < 0)
        {
            fprintf(stderr, "Couldn't open input bits file");
            exit(EXIT_FAILURE);
        }
    }
    else if(!bUseZMQ)
    {
        // read input bits from UDP socket
        struct sockaddr_in addr;
        memset(&addr, 0, sizeof(struct sockaddr_in));
        addr.sin_family = AF_INET;
        addr.sin_port = htons(udpPortRx);
        inet_aton("127.0.0.1", &addr.sin_addr);

        fdInput = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        bind(fdInput, (struct sockaddr *)&addr, sizeof(struct sockaddr));

        printf("Input socket 0x%04x on port %d\n", fdInput, udpPortRx);

        if (fdInput < 0)
        {
            fprintf(stderr, "Couldn't create input socket");
            exit(EXIT_FAILURE);
        }
    }
    else /* use ZMQ */
    {
    	// read input bits from ZMQ
    	//  Socket to talk to clients
   	    context = zmq_ctx_new ();
   	    zmqrx = zmq_socket (context, ZMQ_SUB);
   	    int rc = zmq_connect(zmqrx, "tcp://127.0.0.1:19991");
   	    assert (rc == 0);
   	    rc = zmq_setsockopt (zmqrx, ZMQ_SUBSCRIBE,"",0);
    }

    // create decoder
    Tetra::TetraDecoder * decoder = new Tetra::TetraDecoder(udpSocketFd, bRemoveFillBits, logLevel, bEnableWiresharkOutput);

    // receive buffer
    const int RXBUF_LEN = 1024;
    uint8_t rxBuf[RXBUF_LEN];

    while (!gSigintFlag)
    {
    	int bytesRead;
    	int ofs;
    	memset(rxBuf,0x00,sizeof(rxBuf));
    	if(!bUseZMQ)
    		bytesRead = read(fdInput, rxBuf, sizeof(rxBuf));
    	else
    	{
    		int rcvmore;
    		size_t option_len = sizeof (int);
    		zmq_getsockopt (zmqrx, ZMQ_RCVMORE, &rcvmore, &option_len);
    		bytesRead = zmq_recv (zmqrx, rxBuf, sizeof(rxBuf), 0);
    		if(rcvmore)
    		{
    			ofs = 0;
    		}
    		else
    		{
    			ofs = 3;
    			continue;
    		}
    	}
        if (errno == EINTR)
        {
            // print is required for ^C to be handled
            fprintf(stderr, "EINTR\n");
            break;
        }
        else if (bytesRead < 0)
        {
            fprintf(stderr, "Read error\n");
            break;
        }
        else if (bytesRead == 0)
        {
            break;
        }
	    membuf sb(rxBuf,bytesRead);
	    std::istream iss(&sb);
	    //sb.pubseekoff(ofs,std::ios_base::beg,std::ios_base::out);
        if(bUseZMQ && (size_t)bytesRead>=sizeof(sZMQ_wire_t))
        {
    	    sZMQ_wire_t wireproto;
    	    sb.sgetn((char*)&(wireproto),sizeof(sZMQ_wire_t));
        	if(wireproto.ntags && (wireproto.magic == GR_HEADER_MAGIC))
        	{
        	    gr::tag_t newtag;
        	    uint64_t tags = wireproto.ntags;
        	    while(tags--)
        	    {
        	    	sb.sgetn((char*)&(newtag.offset), sizeof(uint64_t));
        	    	newtag.key = pmt::deserialize(sb);
        	    	newtag.value = pmt::deserialize(sb);
        	    	newtag.srcid = pmt::deserialize(sb);
        	    	std::string key = pmt::symbol_to_string(newtag.key);
        	    	if(pmt::symbol_to_string(newtag.key).compare("ch_freq")==0)
        	    	{
        	    		decoder->resetDecoder();
        	    		decoder->setRXDownlinkFrequency(pmt::to_long(newtag.value));
        	    		std::cout<<"curpos "<<wireproto.offset<<" sz "<< sb.in_avail()<< " tag found @ "<< newtag.offset << " : "<<newtag.key<<" Value: "<<newtag.value<<" Src: "<<newtag.srcid<<std::endl;
        	    	}
        	    }
        	}
        }
        if (programMode & SAVE_TO_BINARY_FILE)
        {
            write(fdOutputSaveFile, rxBuf, bytesRead);
        }

        // bytes must be pushed one at a time into decoder
		//sb.snextc((char*)&(data), sizeof(uint8_t));
        while(sb.in_avail())
        {
        	uint8_t data = sb.snextc();
        	if (programMode & RX_PACKED)
        	{
        		for (uint8_t idx = 0; idx <= 7; idx++)
        	    {
        			decoder->rxSymbol((data >> idx) & 0x01);
        		}
        	}
        	else
        	{
        		decoder->rxSymbol(data);
        	}
        }
/*        for (int cnt = ofs; cnt < bytesRead; cnt++)
        {
        	if (programMode & RX_PACKED)
        	{
        		for (uint8_t idx = 0; idx <= 7; idx++)
        	    {
        			decoder->rxSymbol((rxBuf[cnt] >> idx) & 0x01);
        		}
        	}
        	else
        	{
        		decoder->rxSymbol(rxBuf[cnt]);
        	}
        }
*/
    }

    close(udpSocketFd);

    if(!bUseZMQ)
    {
    	// file or socket must be closed
    	close(fdInput);
    }
    else
    {
    	zmq_close(zmqrx);
    	zmq_ctx_destroy(context);
    }

    // close save file only if openede
    if (programMode & SAVE_TO_BINARY_FILE)
    {
        close(fdOutputSaveFile);
    }

    delete decoder;

    printf("Clean exit\n");

    return EXIT_SUCCESS;
}
