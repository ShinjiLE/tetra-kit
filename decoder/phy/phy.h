#ifndef PHY_H
#define PHY_H
#include <sstream>
#include "../common/tetra.h"
#include "../common/layer.h"
#include "../common/log.h"
#include "../common/report.h"
#include "../common/utils.h"

namespace Tetra {

    class Phy : public Layer {
    public:
        Phy(Log * log, Report * report);
        ~Phy();

        //void service(Pdu pdu, const MacLogicalChannel macLogicalChannel, TetraTime tetraTime, MacAddress macAddress);
    };

};

#endif /* PHY_H */
